-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.20 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for zubnaordinacija
CREATE DATABASE IF NOT EXISTS `zubnaordinacija` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `zubnaordinacija`;

-- Dumping structure for table zubnaordinacija.pacijent
CREATE TABLE IF NOT EXISTS `pacijent` (
  `pacijent_id` int NOT NULL AUTO_INCREMENT,
  `ime` varchar(25) NOT NULL DEFAULT '',
  `prezime` varchar(25) NOT NULL DEFAULT '',
  `broj_telefona` int NOT NULL DEFAULT '0',
  `tip_pregleda` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`pacijent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table zubnaordinacija.pacijent: ~0 rows (approximately)
DELETE FROM `pacijent`;
/*!40000 ALTER TABLE `pacijent` DISABLE KEYS */;
INSERT INTO `pacijent` (`pacijent_id`, `ime`, `prezime`, `broj_telefona`, `tip_pregleda`) VALUES
	(31, 'Milos', 'Cvetkovic', 1111111111, 'qwee');
/*!40000 ALTER TABLE `pacijent` ENABLE KEYS */;

-- Dumping structure for table zubnaordinacija.termin
CREATE TABLE IF NOT EXISTS `termin` (
  `termin_id` int NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `vreme` time NOT NULL,
  `pacijent_id` int NOT NULL,
  PRIMARY KEY (`termin_id`),
  UNIQUE KEY `vreme` (`vreme`),
  KEY `fk_termin_pacijent_id` (`pacijent_id`),
  CONSTRAINT `fk_termin_pacijent_id` FOREIGN KEY (`pacijent_id`) REFERENCES `pacijent` (`pacijent_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table zubnaordinacija.termin: ~0 rows (approximately)
DELETE FROM `termin`;
/*!40000 ALTER TABLE `termin` DISABLE KEYS */;
INSERT INTO `termin` (`termin_id`, `datum`, `vreme`, `pacijent_id`) VALUES
	(21, '2020-10-09', '04:11:00', 31);
/*!40000 ALTER TABLE `termin` ENABLE KEYS */;

-- Dumping structure for table zubnaordinacija.zubar
CREATE TABLE IF NOT EXISTS `zubar` (
  `zubar_id` int NOT NULL,
  `ime` varchar(50) NOT NULL DEFAULT '',
  `prezime` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`zubar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table zubnaordinacija.zubar: ~0 rows (approximately)
DELETE FROM `zubar`;
/*!40000 ALTER TABLE `zubar` DISABLE KEYS */;
/*!40000 ALTER TABLE `zubar` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
